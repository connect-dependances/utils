### omnify-utils

Common utils for Omnify Connect.

### Installation

Install the package

```sh
yarn add @omnify/utils
```

### Docs

See [wiki.omnify.cx](https://wiki.omnify.cx)
